
// Image strip parallax section

$(window).scroll(function(){
  imageStripscroll();
});

function imageStripscroll(){
  var wScroll = $(window).scrollTop();
  $(".strip_one").css('background-position','center -' + wScroll/4 + 'px');
  $(".strip_two").css('background-position','center ' + wScroll/4 + 'px');
  $(".strip_three").css('background-position','center -' + wScroll/4 + 'px');  
  
  if( $( window ).width() >= 310 && $( window ).width()<= 500 ){
    $(".home_header").css('background-size', + wScroll/20+235 + '%');
  }
  else if( $( window ).width() >= 700 && $( window ).width()<= 1024 ){
    $(".home_header").css('background-size', + wScroll/25+195 + '%');
  }
  else{
    $(".home_header").css('background-size', + wScroll/30+100 + '%');
  }
}

// Smooth scroll

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


  jQuery(document).ready(function($) {
              // browser window scroll (in pixels) after which the "back to top" link is shown
              var offset = 300,
                  //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
                  offset_opacity = 1200,
                  //duration of the top scrolling animation (in ms)
                  scroll_top_duration = 700,
                  //grab the "back to top" link
                  $back_to_top = $('.cd-top');
  
              //hide or show the "back to top" link
              $(window).scroll(function() {
                  ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
                  if ($(this).scrollTop() > offset_opacity) {
                      $back_to_top.addClass('cd-fade-out');
                  }
              });
  
          });
